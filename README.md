# Welcome 👋

## Dev 💻
- ♥️ I like Python 🐍 & C/C++ 🅒
- 🌱 I'm learning OCaml 🐫 & Rust 🦀

## Projects ⚒️
### ✨ [CHECK THEM OUT](https://gitlab.com/users/yuesubi/contributed) ✨

## GitHub 
I also have github but I don't use it much : [github.com/yuesubi](https://github.com/yuesubi)
